(function () {
  'use strict';

  Megogo.Views.ContactEdit = Backbone.View.extend({

    el: '#view',

    events: {
      'submit form.contact': 'submit',
      'keyup input': 'updateModel'
    },

    initialize: function () {
      this.model.on('sync', this.render, this);
      this.model.fetch();
    },

    render: function () {
      this.$el.html(_.template($('#contact-edit').html(), {
        contact: this.model
      }));
    },

    updateModel: function (event) {
      this.model.set(event.target.name, event.target.value);
    },

    submit: function (event) {
      event.preventDefault();

      this.model.save(null, {
        success: function() {
          Backbone.history.navigate('/', true);
        }
      });
    }

  });

})();
