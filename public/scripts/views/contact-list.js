(function () {
    'use strict';

    Megogo.Views.ContactList = Backbone.View.extend({

        el: '#view',

        events: {
          'click tr.contact': 'edit'
        },

        initialize: function () {
            this.collection
                .bind('reset', this.render, this)
                .fetch({reset: true});
        },

        render: function () {
            this.$el.html(_.template($('#contact-list').html(), {
                contacts: this.collection.models
            }));
        },

        edit: function (el) {
            Backbone.history.navigate($(el.currentTarget).data('href'), true);
        }
    });

})();
