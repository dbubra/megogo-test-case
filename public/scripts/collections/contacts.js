(function () {
    'use strict';

    Megogo.Collections.Contacts = Backbone.Collection.extend({

        model: Megogo.Models.Contact,

        url: 'api/contacts'

    });
})();
