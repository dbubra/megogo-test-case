(function () {
    'use strict';

    Megogo.Routers.Contacts = Backbone.Router.extend({

        routes: {
            "": "list",
            "edit/:id": "edit"
        },

        list: function() {
            new Megogo.Views.ContactList({
                collection: new Megogo.Collections.Contacts()
            });
        },

        edit: function(id) {
            new Megogo.Views.ContactEdit({
                model: new Megogo.Models.Contact({id: id})
            });
        }

    });

})();
