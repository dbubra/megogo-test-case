(function () {
    'use strict';

    var Megogo = window.Megogo = {
        Models: {},
        Collections: {},
        Views: {},
        Routers: {},
        init: function () {

            new this.Routers.Contacts();

            Backbone.history.start();
        }
    };

    $(function () {
        Megogo.init();
    });

})();
