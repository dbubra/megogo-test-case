(function () {
    'use strict';

    Megogo.Models.Contact = Backbone.Model.extend({

        urlRoot: '/api/contacts',

        defaults: {
            id: null,
            name: '',
            phone: ''
        }
    });

})();
