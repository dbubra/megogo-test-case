var nodeStatic = require('node-static');
var open = require("open");
var _ = require('lodash');

var file = new nodeStatic.Server('./public');

var collection = require('./db/contacts.json');

require('http').createServer(function (req, res) {
  var jsonString = '';

  if (req.method === 'PUT') {
    req.on('data', function (data) {
      jsonString += data;
    });
  }

  req.addListener('end', function () {
    var data = collection;
    var match = req.url.match(/^\/api\/contacts(\/(\d+))?$/);

    if (match) {// api request
      var id = _.parseInt(match[2]);

      if (!_.isNaN(id)) {
        data = _.find(collection, {id: id});

        if (req.method === 'PUT') {
          _.assign(data, _.pick(JSON.parse(jsonString), ['name', 'phone']));
        }
      }

      res.end(JSON.stringify(data));
    } else {
      file.serve(req, res);
    }
  }).resume();
}).listen(3003);

setTimeout(function () {
  open('http://localhost:3003');
}, 1000);

